class EntriesController < ApplicationController
  # resourcify
  before_action :set_entry, only: [:show, :edit, :update, :destroy, :check_in, :get_photos, :get_audits]
  before_action :get_origins, :get_services, only: [:index]
  # GET /entries
  # GET /entries.json
  def index
    @entry = Entry.first
    @entries = []
    if params[:search].present?
      @entries = Entry.all
      if params[:search][:from].present? && params[:search][:to].present?
        @entries = @entries.where(:created_at => params[:search][:from].to_date.beginning_of_day...params[:search][:to].to_date.end_of_day)
      end

      if params[:search][:name].present?
        q = "%#{params[:search][:name]}%"
        pacients = Pacient.where('name_1 ILIKE ? OR name_2 ILIKE ? OR last_name_1 ILIKE ? OR last_name_2 ILIKE ? ', q, q, q, q)
        pacient_ids = pacients.map{|p| p.id}
        @entries = @entries.where(:pacient_id => pacient_ids)
      end
      if params[:search][:order_id].present?
        @entries = @entries.where(:order_id => params[:search][:order_id])
      end
      if params[:search][:document].present?
        pacient = Pacient.where(:identification => params[:search][:document]).last
        pacient_id = pacient.present? ? pacient.id : 0
        @entries = @entries.where(:pacient_id => pacient_id)
      end
      if params[:search][:origin_id].present?
        @entries = @entries.where(:origin_id => params[:search][:origin_id])
      end
      if params[:search][:service_id].present?
        @entries = @entries.where(:service_id => params[:search][:service_id])
      end
      # @entries = @entries.order(id: :desc)
    end
    respond_to do |f|
      f.html {}
      f.js
    end

  end

  # GET /entries/1
  # GET /entries/1.json
  def show
    @entry.save_activity("Ingreso visualizado")
  end

  def search_order
    if params[:search_order].present?
      order_id = params[:search_order][:order_id]
      created_pg_entry = Entry.where(:order_id => order_id).last if order_id.present?
      # First search on pg database

      if created_pg_entry.present?
        @created_entry = created_pg_entry
      else
        @order, @error = get_order_from_sql(order_id)
        @created_entry = Entry.where(:order_id => @order.nro_ord.to_i).last if @order.present?
      end

      if @created_entry.present?
        @entry = @created_entry
      elsif @order.present?
        @entry = Entry.new
        @entry.order_id = @order.nro_ord.to_i
        @entry.description = @order.sin_pac if @order.sin_pac
        # Set Pacient
        pg_pacient = Pacient.where(:sql_id => @order.cod_pac.to_i).last if @order.cod_pac
        if pg_pacient.present?
          pacient = pg_pacient
        else
          pacient = Pacient.new
        end
        
        pacient.identification = @order.doc_pac.strip if @order.doc_pac
        pacient.type_identification = @order.tipo_doc.strip if @order.tipo_doc
        pacient.name_1 = @order.nom_pac if @order.nom_pac
        pacient.name_2 = @order.nom_pac2 if @order.nom_pac2
        pacient.last_name_1 = @order.ape_pac if @order.ape_pac
        pacient.last_name_2 = @order.apm_pac if @order.apm_pac
        pacient.birthday = @order.fna_pac.to_date if @order.fna_pac
        pacient.gender = @order.sex_pac if @order.sex_pac
        pacient.email = @order.email_pac if @order.email_pac
        pacient.phone = @order.tel_pac if @order.tel_pac
        pacient.sql_id = @order.cod_pac.to_i if @order.cod_pac
        @entry.pacient = pacient
        # Set Origin
        pg_origin = Origin.where(:sql_id => @order.cod_ori).last if @order.des_ori
        if pg_origin.present?
          origin = pg_origin
        else
          origin = Origin.new
        end
        origin.name = @order.des_ori if @order.des_ori
        origin.sql_id = @order.cod_ori.to_i if @order.cod_ori
        @entry.origin = origin
        # Set Service
        pg_service = Service.where(:sql_id => @order.cod_ser).last if @order.cod_ser
        if pg_service.present?
          service = pg_service
        else
          service = Service.new
        end
        service.name = @order.des_ser if @order.des_ser
        service.sql_id = @order.cod_ser.to_i if @order.cod_ser
        
        @entry.service = service
      end

    end
  end

  def check_in
    @entry.check_in = true

    # @entry.check_in_by_id = current_user.id
    # @entry.check_in_by_at = Time.now

    # if @entry.save
    #   @entry.save_activity("Ingreso verificado")
    # end

    check_in = CheckIn.new
    check_in.check_in_by_id = current_user.id
    check_in.check_in_at = Time.now
    check_in.note = params[:note] if params[:note].present?
    check_in.entry_id = @entry.id
    if check_in.save
      if params[:"file-snapshot-1"].present?
        photo = PhotoCheckIn.new
        photo.entry_id = @entry.id
        photo.check_in_id = check_in.id
        photo.photo = params[:"file-snapshot-1"]
        photo.save
      end
      @entry.save_activity("Ingreso verificado")
    end
  end

  def get_photos
    @entry
  end

  def get_audits
    @entry
  end

  # GET /entries/new
  def new
    # if current_user.has_role?(:supervisor)
    #   redirect_to entries_path
    # end
    @entry = Entry.new
  end

  # GET /entries/1/edit
  def edit
  end

  # POST /entries
  # POST /entries.json
  def create
    # Rails.logger.info "====>>>>>entry_params: #{entry_params[:image]}"
    @entry = Entry.new(entry_params)
    @entry.created_by_id = current_user.id
    respond_to do |format|
      if @entry.save
        if params[:"file-snapshot-1"].present?
          photo = PhotoEntry.new
          photo.entry_id = @entry.id
          photo.order = 1
          photo.photo = params[:"file-snapshot-1"]
          photo.save
          Rails.logger.info "photo.errors.full_messages: #{photo.errors.full_messages}"
        end

        if params[:"file-snapshot-2"].present?
          photo = PhotoEntry.new
          photo.entry_id = @entry.id
          photo.order = 2
          photo.photo = params[:"file-snapshot-2"]
          photo.save
          Rails.logger.info "photo.errors.full_messages: #{photo.errors.full_messages}"
        end

        if params[:"file-snapshot-3"].present?
          photo = PhotoEntry.new
          photo.entry_id = @entry.id
          photo.order = 3
          photo.photo = params[:"file-snapshot-3"]
          photo.save
          Rails.logger.info "photo.errors.full_messages: #{photo.errors.full_messages}"
        end
        
        @entry.current_user = current_user if current_user
        @entry.ip_address = request.remote_ip
        @entry.save_activity("Ingreso creado")

        format.html { redirect_to @entry, notice: 'Ingreso creado correctamente.' }
        format.json { render :show, status: :created, location: @entry }
      else
        format.html { render :new }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /entries/1
  # PATCH/PUT /entries/1.json
  def update
    respond_to do |format|
      if @entry.update(entry_params)
        if params[:"file-snapshot-1"].present?
          photo = PhotoEntry.new
          photo.entry_id = @entry.id
          photo.order = 1
          photo.photo = params[:"file-snapshot-1"]
          photo.save
          Rails.logger.info "photo.errors.full_messages: #{photo.errors.full_messages}"
        end

        if params[:"file-snapshot-2"].present?
          photo = PhotoEntry.new
          photo.entry_id = @entry.id
          photo.order = 2
          photo.photo = params[:"file-snapshot-2"]
          photo.save
          Rails.logger.info "photo.errors.full_messages: #{photo.errors.full_messages}"
        end

        if params[:"file-snapshot-3"].present?
          photo = PhotoEntry.new
          photo.entry_id = @entry.id
          photo.order = 3
          photo.photo = params[:"file-snapshot-3"]
          photo.save
          Rails.logger.info "photo.errors.full_messages: #{photo.errors.full_messages}"
        end

        format.html { redirect_to @entry, notice: 'Ingreso actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @entry }
      else
        format.html { render :edit }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /entries/1
  # DELETE /entries/1.json
  def destroy
    @entry.destroy
    respond_to do |format|
      format.html { redirect_to entries_url, notice: 'Entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entry
      @entry = Entry.find(params[:id])
      @entry.current_user = current_user if current_user
      @entry.ip_address = request.remote_ip
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entry_params
      params.require(:entry).permit(:pacient_id, :origin_id, :service_id, :from_db, :date_in, :date_out, :check_in, :order_id, :description,
        pacient_attributes: [:id, :sql_id, :identification, :type_identification, :name_1, :name_2, :last_name_1, :last_name_2, :birthday, :gender, :email, :phone, :description, :position, :_destroy],
        origin_attributes: [:id, :sql_id, :name, :_destroy],
        service_attributes: [:id, :sql_id, :name, :_destroy] )
    end
end
