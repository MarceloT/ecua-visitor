class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  before_action :set_current_user, :set_date_format
  def set_date_format
    @date_format = "%d-%m-%Y"
    @date_format_words = "%A, %-d %b %Y"
    @date_time_format_words = "%A, %-d %b %Y %I:%M"
    @date_time_format = "%d-%m-%Y %I:%M"
  end

  def set_current_user
    @current_user = current_user
  end

  def get_order_from_sql(order_id)
    begin
      Rails.logger.info "ApplicationController#get_order_from_sql"
      return nil, "Ingrese número de orden" unless order_id.present?      
      client = TinyTds::Client.new username: 'sa', password: 'sql', host: '192.168.57.4', database: 'nextlab', login_timeout: 5
      Rails.logger.info "client.active?:#{client.active?}"
      if client.active?
        sql_query = "select o.nro_ord, o.sin_pac, o.fec_ord, o.cod_pac, o.cod_ori, ori.des_ori, o.cod_ser,ser.des_ser, p.doc_pac, p.nom_pac, p.nom_pac2, p.ape_pac, p.apm_pac, p.fna_pac, p.sex_pac, p.email_pac, p.tel_pac, p.tipo_doc from lisord as o join lispac as p on o.cod_pac = p.cod_pac join lisori as ori on o.cod_ori = ori.cod_ori join lisser as ser on o.cod_ser = ser.cod_ser where o.nro_ord = '#{order_id}'"
        Rails.logger.info "sql_query:#{sql_query}"
        result = client.execute(sql_query)
        order = result.each(:first => true)[0]
        if order.present?
          order = OpenStruct.new(order)
          return order, nil
        else
          return nil, "Orden no encontrada en NEXTLAB"
        end
      else
        return nil, nil
      end  
    rescue Exception => e
      Rails.logger.info "error:#{e}"
      return nil, e
    end
  end

  def get_origins
    @origins = Origin.all
  end

  def get_services
    @services = Service.all
  end

  def get_roles
    @all_roles = Role.where(:name => ["receptionist", "supervisor"]).order(:name)
  end

  def get_stations
    @stations = [["Matriz - Puesto 1","Matriz - Puesto 1"],
                ["Matriz - Puesto 2","Matriz - Puesto 2"],
                ["Matriz - Puesto 3","Matriz - Puesto 3"],
                ["Matriz - Puesto 4","Matriz - Puesto 4"], 
                ["Matriz - Puesto 5","Matriz - Puesto 5"], 
                ["Matriz - Puesto 6","Matriz - Puesto 6"], 
                ["Matriz - Puesto 7","Matriz - Puesto 7"], 
                ["Matriz - Puesto 8","Matriz - Puesto 8"],
                ["Matriz - Puesto 9","Matriz - Puesto 9"], 
                ["Matriz - Puesto 10","Matriz - Puesto 10"], 
                ["Matriz - Puesto 11","Matriz - Puesto 11"], 
                ["Matriz - Puesto 12","Matriz - Puesto 12"]]

    used_stations =  User.all.map{|u| [u.station_name, u.station_name]}
    @stations = @stations - used_stations
  end

end
