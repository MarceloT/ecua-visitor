class PhotoCheckInsController < ApplicationController
  before_action :set_photo_check_in, only: [:show, :edit, :update, :destroy]

  # GET /photo_check_ins
  # GET /photo_check_ins.json
  def index
    @photo_check_ins = PhotoCheckIn.all
  end

  # GET /photo_check_ins/1
  # GET /photo_check_ins/1.json
  def show
  end

  # GET /photo_check_ins/new
  def new
    @photo_check_in = PhotoCheckIn.new
  end

  # GET /photo_check_ins/1/edit
  def edit
  end

  # POST /photo_check_ins
  # POST /photo_check_ins.json
  def create
    @photo_check_in = PhotoCheckIn.new(photo_check_in_params)

    respond_to do |format|
      if @photo_check_in.save
        format.html { redirect_to @photo_check_in, notice: 'Photo check in was successfully created.' }
        format.json { render :show, status: :created, location: @photo_check_in }
      else
        format.html { render :new }
        format.json { render json: @photo_check_in.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /photo_check_ins/1
  # PATCH/PUT /photo_check_ins/1.json
  def update
    respond_to do |format|
      if @photo_check_in.update(photo_check_in_params)
        format.html { redirect_to @photo_check_in, notice: 'Photo check in was successfully updated.' }
        format.json { render :show, status: :ok, location: @photo_check_in }
      else
        format.html { render :edit }
        format.json { render json: @photo_check_in.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /photo_check_ins/1
  # DELETE /photo_check_ins/1.json
  def destroy
    @photo_check_in.destroy
    respond_to do |format|
      format.html { redirect_to photo_check_ins_url, notice: 'Photo check in was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_photo_check_in
      @photo_check_in = PhotoCheckIn.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def photo_check_in_params
      params.require(:photo_check_in).permit(:entry_id, :check_in_id, :photo)
    end
end
