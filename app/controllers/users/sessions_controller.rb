# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]
  before_action :get_stations
  layout "blank"
  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  def create
    user = User.where(:login => params[:user][:login]).last
    if user.present? && user.has_role?(:receptionist)
      user.station_name = params[:user][:station_name]
      user.save
    end
    super
  end

  # DELETE /resource/sign_out
  def destroy
    user = current_user
    user.station_name = nil
    user.save
    super
  end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
