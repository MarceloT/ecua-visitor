class PhotoEntriesController < ApplicationController
  before_action :set_photo_entry, only: [:show, :edit, :update, :destroy]

  # GET /photo_entries
  # GET /photo_entries.json
  def index
    @photo_entries = PhotoEntry.all
  end

  # GET /photo_entries/1
  # GET /photo_entries/1.json
  def show
  end

  # GET /photo_entries/new
  def new
    @photo_entry = PhotoEntry.new
  end

  # GET /photo_entries/1/edit
  def edit
  end

  # POST /photo_entries
  # POST /photo_entries.json
  def create
    @photo_entry = PhotoEntry.new(photo_entry_params)

    respond_to do |format|
      if @photo_entry.save
        format.html { redirect_to @photo_entry, notice: 'Photo entry was successfully created.' }
        format.json { render :show, status: :created, location: @photo_entry }
      else
        format.html { render :new }
        format.json { render json: @photo_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /photo_entries/1
  # PATCH/PUT /photo_entries/1.json
  def update
    respond_to do |format|
      if @photo_entry.update(photo_entry_params)
        format.html { redirect_to @photo_entry, notice: 'Photo entry was successfully updated.' }
        format.json { render :show, status: :ok, location: @photo_entry }
      else
        format.html { render :edit }
        format.json { render json: @photo_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /photo_entries/1
  # DELETE /photo_entries/1.json
  def destroy
    @photo_entry.destroy
    respond_to do |format|
      format.html { redirect_to photo_entries_url, notice: 'Photo entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_photo_entry
      @photo_entry = PhotoEntry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def photo_entry_params
      params.require(:photo_entry).permit(:photo, :entry_id)
    end
end
