class PhotoEntry < ApplicationRecord
	belongs_to :entry, class_name: "Entry", foreign_key: "entry_id"
	mount_base64_uploader :photo, PhotoUploader
end
