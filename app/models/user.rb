class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable, :registerable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable, :trackable, authentication_keys: [:login]

  validates :login, presence: true, uniqueness: true
  validates :name, presence: true, uniqueness: true

  attr_writer :login_user
  
  def login_user
    @login_user || self.login || self.email
  end

  def email_required?
	  false
	end

	def role_name
    if self.roles.first.present?
      if self.roles.first.name == "admin"
        "Administrador del Sistema"
      elsif self.roles.first.name == "supervisor"
        "Supervisor"
      elsif self.roles.first.name == "receptionist"
        "Recepcionista"
      end
    else
      "Sin Asignar"
    end
  end

end
