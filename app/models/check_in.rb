class CheckIn < ApplicationRecord
	belongs_to :entry, class_name: "Entry", foreign_key: "entry_id"
	belongs_to :check_in_by, class_name: "User", foreign_key: "check_in_by_id",:optional => true

	has_many :photo_check_ins
  accepts_nested_attributes_for :photo_check_ins
end
