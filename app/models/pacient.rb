class Pacient < ApplicationRecord
	def full_name
		"#{self.name_1} #{self.name_2} #{self.last_name_1} #{self.last_name_2}"
	end

	def age
    # self.birthday.present? ? Time.now.year - self.birthday.year : 0
   	#  dob = self.birthday
   	#  now = Time.zone.now
  	# now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
  	self.birthday.present? ? ((Time.zone.now - self.birthday.to_time) / 1.year.seconds).floor : 0
  end
end
