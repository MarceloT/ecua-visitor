class Entry < ApplicationRecord
	belongs_to :pacient, class_name: "Pacient", foreign_key: "pacient_id"
	accepts_nested_attributes_for :pacient
	belongs_to :origin, class_name: "Origin", foreign_key: "origin_id"
	accepts_nested_attributes_for :origin
	belongs_to :service, class_name: "Service", foreign_key: "service_id"
	accepts_nested_attributes_for :service
	
	belongs_to :created_by, class_name: "User", foreign_key: "created_by_id", :optional => true
	belongs_to :check_in_by, class_name: "User", foreign_key: "check_in_by_id",:optional => true

	has_many :photo_entries
  accepts_nested_attributes_for :photo_entries

  has_many :check_ins
  accepts_nested_attributes_for :check_ins

  has_many :audits

  validates :order_id, presence: true, uniqueness: true

  # after_save :save_activity

  attr_accessor :current_user, :ip_address

  def save_activity(action)
  	# Ingreso creado
  	# Ingreso verificado
  	# Ingreso visualizado

  	audit = Audit.new
  	audit.user_id = self.current_user.id if self.current_user
  	audit.entry_id = self.id
  	audit.ip = self.ip_address
  	audit.action = action
  	audit.date = Time.now
  	audit.save
  end
end
