class PhotoCheckIn < ApplicationRecord
	belongs_to :entry, class_name: "Entry", foreign_key: "entry_id"
	belongs_to :check_id, class_name: "CheckIn", foreign_key: "check_in_id"
	mount_base64_uploader :photo, PhotoUploader
end
