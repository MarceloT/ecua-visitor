//========= List Entries =========
// load_timeline();
$(document).on("turbolinks:load",function(){
	function load_timeline(){
	  console.log("Load entries...");
	  // $("#container-entries").hide();
	  $("#records_spinner").show();
	  if ($("#send_load_records").length > 0 ){
	    $("#send_load_records").click();
	  }
	}
	$( "#search" ).on( "click", function(event) {
	  event.preventDefault();
	  console.log("click on search");
	  load_timeline();  
	});
	$( "#today" ).on( "click", function(event) {
	  event.preventDefault();
	  search_from.setDate(new Date(), "Y-m-d");
	  search_to.setDate(new Date(), "Y-m-d");
	  load_timeline();
	});

	$( "#week" ).on( "click", function(event) {
	  event.preventDefault();
	  var dt = new Date()  //current date of week
	  var currentWeekDay = dt.getDay();
	  var lessDays = currentWeekDay == 0 ? 6 : currentWeekDay-1
	  var wkStart = new Date(new Date(dt).setDate(dt.getDate()- lessDays));
	  var wkEnd = new Date(new Date(wkStart).setDate(wkStart.getDate()+6));
	  search_from.setDate(wkStart, "Y-m-d");
	  search_to.setDate(wkEnd, "Y-m-d");
	  load_timeline();
	});

	$( "#month" ).on( "click", function(event) {
	  event.preventDefault();
	  var date = new Date(); 
	  var firstDay = new Date(date.getFullYear(), date.getMonth(), 1); 
	  var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
	  search_from.setDate(firstDay, "Y-m-d");
	  search_to.setDate(lastDay, "Y-m-d");
	  load_timeline();
	});

	$("#search_origin_id, #search_service_id").change(function() {
	  load_timeline();    
	});

	$("#search_origin_id, #search_service_id").change(function() {
	  load_timeline();    
	});

	if ($("#search_order_id").val()) {
	  load_timeline();    
	};  

	//========= New Entry =========
	$( "#search_order" ).on( "click", function(event) {
	  event.preventDefault();
	  console.log("click on search order");
	  // load_timeline();  
	});

	$( "#reset_load_order" ).on( "click", function(event) {
	  event.preventDefault();
	  console.log("click on clean_order");
	  $("#entry_form").html("");
	});
	//========= Load Camera =========
	var width = 0;
	var id_preview = "";
	  
	function setCamera(){
	  width = $("#camera-modal").width();
	  // width = width - 15;

	  width_crop = 350;
	  Webcam.set({
	    width: width,
	    height: width / 1.333333333,
	    crop_width: width_crop,
	    crop_height: width_crop * 1.2,
	    image_format: 'png',
	    jpeg_quality: 100
	  });
	  Webcam.attach( '#camera' );
	}

	

	// $(".take-photo").on( "click", function(event) {
	//   event.preventDefault();
	//   console.log($(this).attr("id"));
	// });

	$('.modal.webcam').on('shown.bs.modal', function (e) {
	  loadCamera();
	})

	$('.modal.webcam').on('hidden.bs.modal', function (e) {
	  destroyCamera();
	})

	function loadCamera(){
	  console.log("loadCamera");
	  setCamera();
	  openCamera();
	}

	function destroyCamera(){
	  console.log("destroyCamera");
	  Webcam.reset();
	  $("#results").hide();
	  $("#camera").hide();
	}

	// preload shutter audio clip
	// var shutter = new Audio();
	// shutter.autoplay = true;
	// shutter.src = navigator.userAgent.match(/Firefox/) ? '/webcamjs/shutter.ogg' : '/webcamjs/shutter.mp3';
	
})

function set_preview_id(button){
  console.log(button.attr("id"));
  id_preview = "preview-" + button.attr("id");
}

function run_snapshot(){
  // play sound effect
  // shutter.play();
  // take snapshot and get image data
  Webcam.snap( function(data_uri) {
    // display results in page
    document.getElementById('results').innerHTML = 
      '<img src="'+data_uri+'"/>';

    console.log(id_preview);
    $(".container-image#"+ id_preview +" img").attr("src", data_uri);
    // $(".container-image#file-snapshot-1").val(data_uri);
    $("." + id_preview.replace("preview", "file")).val(data_uri);
      
  });
  
  $("#results").show();
  $("#camera").hide();

  $("#btn-snapshot").hide();
  $("#btn-resnapshot").show();
  $("#btn-close").show();
}

function openCamera(){
  $("#camera").show();
  $("#results").hide();

  $("#btn-snapshot").show();
  $("#btn-resnapshot").hide();
  $("#btn-close").hide();
}
