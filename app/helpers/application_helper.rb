module ApplicationHelper
	def get_age(birthday)
		((Time.zone.now - birthday.to_time) / 1.year.seconds).floor
	end
end
