class PhotoUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/entries/#{model.entry_id}"
    # "uploads/entries/#{model.entry_id}/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url(*args)
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process scale: [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  version :thumb do
    process resize_to_fit: [150, 150]
  end

  version :zoom do
    process resize_to_fit: [680, 816]
  end

  # version :best do
  #   Rails.logger.debug " Running version BEST at #{DateTime.now}"
  #   process :get_geometry
  #   def geometry
  #     @geometry
  #   end
  # end

  # def get_geometry
  #   # Rails.logger.debug "Running get_geometry at #{DateTime.now()}"
  #   if @file
  #     file_url_new = @file.file.to_s
  #     img = MiniMagick::Image.open(file_url_new)
  #     @geometry = img["dimensions"]
  #     img = nil
  #   end
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # def extension_whitelist
  #   %w(jpg jpeg gif png)
  # end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  def filename
    # "something.jpg" if original_filename
    if version_name.present?
      "#{version_name}_#{model.class.to_s.underscore}_#{model.id.to_s}.png"
    else
      "best_#{model.class.to_s.underscore}_#{model.id.to_s}.png"
    end
  end

  def full_filename(for_file)
    # "something.jpg" if original_filename
    if version_name.present?
      "#{version_name}_#{model.class.to_s.underscore}_#{model.id.to_s}.png"
    else
      "best_#{model.class.to_s.underscore}_#{model.id.to_s}.png"
    end
  end
end
