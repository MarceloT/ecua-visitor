json.extract! photo_check_in, :id, :entry_id, :check_in_id, :photo, :created_at, :updated_at
json.url photo_check_in_url(photo_check_in, format: :json)
