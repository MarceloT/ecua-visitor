json.extract! photo_entry, :id, :photo, :entry_id, :created_at, :updated_at
json.url photo_entry_url(photo_entry, format: :json)
