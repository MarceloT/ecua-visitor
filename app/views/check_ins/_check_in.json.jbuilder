json.extract! check_in, :id, :entry_id, :check_in_by_id, :check_in_at, :note, :created_at, :updated_at
json.url check_in_url(check_in, format: :json)
