json.extract! audit, :id, :user_id, :entry_id, :ip, :action, :date, :created_at, :updated_at
json.url audit_url(audit, format: :json)
