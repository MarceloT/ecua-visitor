json.extract! entry, :id, :pacient_id, :origin_id, :service_id, :from_db, :date_in, :date_out, :check_in, :created_at, :updated_at
json.url entry_url(entry, format: :json)
