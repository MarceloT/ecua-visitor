json.extract! pacient, :id, :identification, :type_identification, :name_1, :name_2, :last_name_1, :last_name_2, :birthday, :gender, :email, :phone, :description, :position, :created_at, :updated_at
json.url pacient_url(pacient, format: :json)
