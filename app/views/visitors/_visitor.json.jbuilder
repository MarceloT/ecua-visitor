json.extract! visitor, :id, :name, :ci, :company_id, :email, :direction, :gender_id, :birthday, :photo, :signature, :created_at, :updated_at
json.url visitor_url(visitor, format: :json)
