# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create(login: 'recep', email: "recep@ecua-american.com", password: "123456", name: "Recepcionista", active: true)
user.add_role "receptionist"
user = User.create(login: 'super', email: "super@ecua-american.com", password: "123456", name: "Supervisor", active: true)
user.add_role "supervisor"
user = User.create(login: 'admin', email: "admin@ecua-american.com", password: "123456", name: "Administrador del Sistema", active: true)
user.add_role "admin"
