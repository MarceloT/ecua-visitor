class CreateAudits < ActiveRecord::Migration[5.2]
  def change
    create_table :audits do |t|
      t.integer :user_id
      t.integer :entry_id
      t.string :ip
      t.text :action
      t.datetime :date

      t.timestamps
    end
  end
end
