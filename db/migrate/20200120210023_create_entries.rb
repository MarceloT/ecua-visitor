class CreateEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :entries do |t|
      t.integer :pacient_id
      t.integer :origin_id
      t.integer :service_id
      t.boolean :from_db
      t.datetime :date_in
      t.datetime :date_out
      t.boolean :check_in

      t.timestamps
    end
  end
end
