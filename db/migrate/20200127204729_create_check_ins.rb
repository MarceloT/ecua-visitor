class CreateCheckIns < ActiveRecord::Migration[5.2]
  def change
    create_table :check_ins do |t|
      t.integer :entry_id
      t.integer :check_in_by_id
      t.datetime :check_in_at
      t.text :note

      t.timestamps
    end
  end
end
