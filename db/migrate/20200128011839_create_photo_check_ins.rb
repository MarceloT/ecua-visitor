class CreatePhotoCheckIns < ActiveRecord::Migration[5.2]
  def change
    create_table :photo_check_ins do |t|
      t.integer :entry_id
      t.integer :check_in_id
      t.string :photo

      t.timestamps
    end
  end
end
