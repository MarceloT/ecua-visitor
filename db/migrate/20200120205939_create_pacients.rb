class CreatePacients < ActiveRecord::Migration[5.2]
  def change
    create_table :pacients do |t|
      t.string :identification
      t.string :type_identification
      t.string :name_1
      t.string :name_2
      t.string :last_name_1
      t.string :last_name_2
      t.datetime :birthday
      t.string :gender
      t.string :email
      t.string :phone
      t.text :description
      t.string :position

      t.timestamps
    end
  end
end
