class AddOrderIdToEntries < ActiveRecord::Migration[5.2]
  def change
  	add_column :entries, :order_id, :integer
  	add_column :origins, :sql_id, :integer
  	add_column :services, :sql_id, :integer
  	add_column :pacients, :sql_id, :integer
  end
end
