class AddOrderToPhotos < ActiveRecord::Migration[5.2]
  def change
  	add_column :photo_entries, :order, :integer
  end
end
