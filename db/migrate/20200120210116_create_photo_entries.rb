class CreatePhotoEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :photo_entries do |t|
      t.string :photo
      t.integer :entry_id

      t.timestamps
    end
  end
end
