class CreateVisitors < ActiveRecord::Migration[5.2]
  def change
    create_table :visitors do |t|
      t.string :name
      t.string :ci
      t.integer :company_id
      t.string :email
      t.string :direction
      t.integer :gender_id
      t.datetime :birthday
      t.string :photo
      t.string :signature

      t.timestamps
    end
  end
end
