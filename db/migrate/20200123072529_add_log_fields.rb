class AddLogFields < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :station_name, :string
  	add_column :entries, :check_in_by_id, :integer
  	add_column :entries, :created_by_id, :integer
  	add_column :entries, :check_in_by_at, :datetime
  	add_column :entries, :description, :text
  	add_column :entries, :type_pacient, :string
  	add_column :entries, :position_origin, :string
  end
end
