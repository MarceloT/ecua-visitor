require "application_system_test_case"

class PhotoCheckInsTest < ApplicationSystemTestCase
  setup do
    @photo_check_in = photo_check_ins(:one)
  end

  test "visiting the index" do
    visit photo_check_ins_url
    assert_selector "h1", text: "Photo Check Ins"
  end

  test "creating a Photo check in" do
    visit photo_check_ins_url
    click_on "New Photo Check In"

    fill_in "Check in", with: @photo_check_in.check_in_id
    fill_in "Entry", with: @photo_check_in.entry_id
    fill_in "Photo", with: @photo_check_in.photo
    click_on "Create Photo check in"

    assert_text "Photo check in was successfully created"
    click_on "Back"
  end

  test "updating a Photo check in" do
    visit photo_check_ins_url
    click_on "Edit", match: :first

    fill_in "Check in", with: @photo_check_in.check_in_id
    fill_in "Entry", with: @photo_check_in.entry_id
    fill_in "Photo", with: @photo_check_in.photo
    click_on "Update Photo check in"

    assert_text "Photo check in was successfully updated"
    click_on "Back"
  end

  test "destroying a Photo check in" do
    visit photo_check_ins_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Photo check in was successfully destroyed"
  end
end
