require "application_system_test_case"

class PacientsTest < ApplicationSystemTestCase
  setup do
    @pacient = pacients(:one)
  end

  test "visiting the index" do
    visit pacients_url
    assert_selector "h1", text: "Pacients"
  end

  test "creating a Pacient" do
    visit pacients_url
    click_on "New Pacient"

    fill_in "Birthday", with: @pacient.birthday
    fill_in "Description", with: @pacient.description
    fill_in "Email", with: @pacient.email
    fill_in "Gender", with: @pacient.gender
    fill_in "Identification", with: @pacient.identification
    fill_in "Last name 1", with: @pacient.last_name_1
    fill_in "Last name 2", with: @pacient.last_name_2
    fill_in "Name 1", with: @pacient.name_1
    fill_in "Name 2", with: @pacient.name_2
    fill_in "Phone", with: @pacient.phone
    fill_in "Position", with: @pacient.position
    fill_in "Type identification", with: @pacient.type_identification
    click_on "Create Pacient"

    assert_text "Pacient was successfully created"
    click_on "Back"
  end

  test "updating a Pacient" do
    visit pacients_url
    click_on "Edit", match: :first

    fill_in "Birthday", with: @pacient.birthday
    fill_in "Description", with: @pacient.description
    fill_in "Email", with: @pacient.email
    fill_in "Gender", with: @pacient.gender
    fill_in "Identification", with: @pacient.identification
    fill_in "Last name 1", with: @pacient.last_name_1
    fill_in "Last name 2", with: @pacient.last_name_2
    fill_in "Name 1", with: @pacient.name_1
    fill_in "Name 2", with: @pacient.name_2
    fill_in "Phone", with: @pacient.phone
    fill_in "Position", with: @pacient.position
    fill_in "Type identification", with: @pacient.type_identification
    click_on "Update Pacient"

    assert_text "Pacient was successfully updated"
    click_on "Back"
  end

  test "destroying a Pacient" do
    visit pacients_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Pacient was successfully destroyed"
  end
end
