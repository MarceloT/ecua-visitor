require "application_system_test_case"

class CheckInsTest < ApplicationSystemTestCase
  setup do
    @check_in = check_ins(:one)
  end

  test "visiting the index" do
    visit check_ins_url
    assert_selector "h1", text: "Check Ins"
  end

  test "creating a Check in" do
    visit check_ins_url
    click_on "New Check In"

    fill_in "Check in at", with: @check_in.check_in_at
    fill_in "Check in by", with: @check_in.check_in_by_id
    fill_in "Entry", with: @check_in.entry_id
    fill_in "Note", with: @check_in.note
    click_on "Create Check in"

    assert_text "Check in was successfully created"
    click_on "Back"
  end

  test "updating a Check in" do
    visit check_ins_url
    click_on "Edit", match: :first

    fill_in "Check in at", with: @check_in.check_in_at
    fill_in "Check in by", with: @check_in.check_in_by_id
    fill_in "Entry", with: @check_in.entry_id
    fill_in "Note", with: @check_in.note
    click_on "Update Check in"

    assert_text "Check in was successfully updated"
    click_on "Back"
  end

  test "destroying a Check in" do
    visit check_ins_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Check in was successfully destroyed"
  end
end
