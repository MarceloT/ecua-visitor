require "application_system_test_case"

class EntriesTest < ApplicationSystemTestCase
  setup do
    @entry = entries(:one)
  end

  test "visiting the index" do
    visit entries_url
    assert_selector "h1", text: "Entries"
  end

  test "creating a Entry" do
    visit entries_url
    click_on "New Entry"

    check "Check in" if @entry.check_in
    fill_in "Date in", with: @entry.date_in
    fill_in "Date out", with: @entry.date_out
    check "From db" if @entry.from_db
    fill_in "Origin", with: @entry.origin_id
    fill_in "Pacient", with: @entry.pacient_id
    fill_in "Service", with: @entry.service_id
    click_on "Create Entry"

    assert_text "Entry was successfully created"
    click_on "Back"
  end

  test "updating a Entry" do
    visit entries_url
    click_on "Edit", match: :first

    check "Check in" if @entry.check_in
    fill_in "Date in", with: @entry.date_in
    fill_in "Date out", with: @entry.date_out
    check "From db" if @entry.from_db
    fill_in "Origin", with: @entry.origin_id
    fill_in "Pacient", with: @entry.pacient_id
    fill_in "Service", with: @entry.service_id
    click_on "Update Entry"

    assert_text "Entry was successfully updated"
    click_on "Back"
  end

  test "destroying a Entry" do
    visit entries_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Entry was successfully destroyed"
  end
end
