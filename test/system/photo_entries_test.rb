require "application_system_test_case"

class PhotoEntriesTest < ApplicationSystemTestCase
  setup do
    @photo_entry = photo_entries(:one)
  end

  test "visiting the index" do
    visit photo_entries_url
    assert_selector "h1", text: "Photo Entries"
  end

  test "creating a Photo entry" do
    visit photo_entries_url
    click_on "New Photo Entry"

    fill_in "Entry", with: @photo_entry.entry_id
    fill_in "Photo", with: @photo_entry.photo
    click_on "Create Photo entry"

    assert_text "Photo entry was successfully created"
    click_on "Back"
  end

  test "updating a Photo entry" do
    visit photo_entries_url
    click_on "Edit", match: :first

    fill_in "Entry", with: @photo_entry.entry_id
    fill_in "Photo", with: @photo_entry.photo
    click_on "Update Photo entry"

    assert_text "Photo entry was successfully updated"
    click_on "Back"
  end

  test "destroying a Photo entry" do
    visit photo_entries_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Photo entry was successfully destroyed"
  end
end
