require 'test_helper'

class PhotoEntriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @photo_entry = photo_entries(:one)
  end

  test "should get index" do
    get photo_entries_url
    assert_response :success
  end

  test "should get new" do
    get new_photo_entry_url
    assert_response :success
  end

  test "should create photo_entry" do
    assert_difference('PhotoEntry.count') do
      post photo_entries_url, params: { photo_entry: { entry_id: @photo_entry.entry_id, photo: @photo_entry.photo } }
    end

    assert_redirected_to photo_entry_url(PhotoEntry.last)
  end

  test "should show photo_entry" do
    get photo_entry_url(@photo_entry)
    assert_response :success
  end

  test "should get edit" do
    get edit_photo_entry_url(@photo_entry)
    assert_response :success
  end

  test "should update photo_entry" do
    patch photo_entry_url(@photo_entry), params: { photo_entry: { entry_id: @photo_entry.entry_id, photo: @photo_entry.photo } }
    assert_redirected_to photo_entry_url(@photo_entry)
  end

  test "should destroy photo_entry" do
    assert_difference('PhotoEntry.count', -1) do
      delete photo_entry_url(@photo_entry)
    end

    assert_redirected_to photo_entries_url
  end
end
