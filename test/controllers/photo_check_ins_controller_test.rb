require 'test_helper'

class PhotoCheckInsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @photo_check_in = photo_check_ins(:one)
  end

  test "should get index" do
    get photo_check_ins_url
    assert_response :success
  end

  test "should get new" do
    get new_photo_check_in_url
    assert_response :success
  end

  test "should create photo_check_in" do
    assert_difference('PhotoCheckIn.count') do
      post photo_check_ins_url, params: { photo_check_in: { check_in_id: @photo_check_in.check_in_id, entry_id: @photo_check_in.entry_id, photo: @photo_check_in.photo } }
    end

    assert_redirected_to photo_check_in_url(PhotoCheckIn.last)
  end

  test "should show photo_check_in" do
    get photo_check_in_url(@photo_check_in)
    assert_response :success
  end

  test "should get edit" do
    get edit_photo_check_in_url(@photo_check_in)
    assert_response :success
  end

  test "should update photo_check_in" do
    patch photo_check_in_url(@photo_check_in), params: { photo_check_in: { check_in_id: @photo_check_in.check_in_id, entry_id: @photo_check_in.entry_id, photo: @photo_check_in.photo } }
    assert_redirected_to photo_check_in_url(@photo_check_in)
  end

  test "should destroy photo_check_in" do
    assert_difference('PhotoCheckIn.count', -1) do
      delete photo_check_in_url(@photo_check_in)
    end

    assert_redirected_to photo_check_ins_url
  end
end
