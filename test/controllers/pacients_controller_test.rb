require 'test_helper'

class PacientsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pacient = pacients(:one)
  end

  test "should get index" do
    get pacients_url
    assert_response :success
  end

  test "should get new" do
    get new_pacient_url
    assert_response :success
  end

  test "should create pacient" do
    assert_difference('Pacient.count') do
      post pacients_url, params: { pacient: { birthday: @pacient.birthday, description: @pacient.description, email: @pacient.email, gender: @pacient.gender, identification: @pacient.identification, last_name_1: @pacient.last_name_1, last_name_2: @pacient.last_name_2, name_1: @pacient.name_1, name_2: @pacient.name_2, phone: @pacient.phone, position: @pacient.position, type_identification: @pacient.type_identification } }
    end

    assert_redirected_to pacient_url(Pacient.last)
  end

  test "should show pacient" do
    get pacient_url(@pacient)
    assert_response :success
  end

  test "should get edit" do
    get edit_pacient_url(@pacient)
    assert_response :success
  end

  test "should update pacient" do
    patch pacient_url(@pacient), params: { pacient: { birthday: @pacient.birthday, description: @pacient.description, email: @pacient.email, gender: @pacient.gender, identification: @pacient.identification, last_name_1: @pacient.last_name_1, last_name_2: @pacient.last_name_2, name_1: @pacient.name_1, name_2: @pacient.name_2, phone: @pacient.phone, position: @pacient.position, type_identification: @pacient.type_identification } }
    assert_redirected_to pacient_url(@pacient)
  end

  test "should destroy pacient" do
    assert_difference('Pacient.count', -1) do
      delete pacient_url(@pacient)
    end

    assert_redirected_to pacients_url
  end
end
