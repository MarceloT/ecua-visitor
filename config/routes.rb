Rails.application.routes.draw do
  resources :photo_check_ins
  resources :check_ins
  resources :audits
  devise_for :users, controllers: { sessions: 'users/sessions' }

  resources :photo_entries
  resources :services
  resources :origins
  resources :pacients
  resources :visitors
  resources :users
  match '/search_order' => 'entries#search_order', :via => [:get, :post, :put]
  
  get 'home/index'
  get 'vistors/index'
  root to: 'entries#new'
  # root to: 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :entries do
    member do 
      post 'check_in'
      get 'get_photos'
      get 'get_audits'
    end
  end
end
